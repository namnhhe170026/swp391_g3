<%@page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Change Password - Midway Transaction Guard</title>
    <link rel="shortcut icon" type="image/icon" href="/Test/assets/logo/logo.png"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        body {
            color: #fff;
            background: #3598dc;
        }

        .form-control {
            min-height: 41px;
            background: #f2f2f2;
            box-shadow: none !important;
            border: transparent;
        }

        .form-control:focus {
            background: #e2e2e2;
        }

        .form-control,
        .btn {
            border-radius: 2px;
        }

        /* Additional style for the change password form */
        .change-password-form {
            width: 350px;
            margin: 30px auto;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="change-password-form">
        <form action="authen?action=changePassword" method="post">
            <h2 class="text-center">Thay Đổi Mật Khẩu</h2>
            <div class="form-group has-error">
                <input type="text" class="form-control" name="username" placeholder="Tên tài khoản" required="required">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="opassword" placeholder="Mật Khẩu Cũ" required="required">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="npassword" placeholder="Mật Khẩu Mới" required="required">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="cfpassword" placeholder="Nhập Lại Mật Khẩu Mới" required="required">
            </div>
            <div class="form-group">
                <div style="color:red">${error}</div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Thay Đổi Mật Khẩu</button>
            </div>
            
        </form>
    </div>
</body>

</html>

