<%@page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Quên Mật Khẩu - Midway Transaction Guard</title>
    <link rel="shortcut icon" type="image/icon" href="/Test/assets/logo/logo.png"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        /* Same styling as the login page */

        /* Additional style for the forgot password form */
        .forgot-password-form {
            width: 350px;
            margin: 30px auto;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="forgot-password-form">
        <form action="authen?action=forgotPassword" method="post">
            <h2 class="text-center">Quên Mật Khẩu</h2>
            <div class="form-group">
                <input type="text" class="form-control" name="username" placeholder="Tên tài khoản" required="required">
            </div>
            <div class="form-group">
                <div style="color:red">${error}</div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Yêu Cầu Cấp Lại Mật Khẩu</button>
            </div>
            <p>
                <a href="${pageContext.request.contextPath}/authen?action=login">Quay lại đăng nhập</a>
            </p>
        </form>
    </div>
</body>

</html>

