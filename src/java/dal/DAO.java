/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Users;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class DAO {
    public static  DAO INSTANCE = new DAO();
    private Connection con;
    private String status;

    private DAO() {
        try {
             DBContext dbContext = new DBContext();
            con = dbContext.makeConnection();
        } catch (ClassNotFoundException | SQLException e) {
            status = "Error at Connection" + e.getMessage();
        }
    }

    public static DAO getInstance() {
        return INSTANCE;
    }

    public Connection getConnection() {
        return con;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Users Login(String username, String password) {
        String sql = "SELECT * FROM Users WHERE Username = ? AND Password = ? AND IsDeleted = 0";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, username);
        ps.setString(2, password);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return new Users(
                rs.getInt("ID"),
                rs.getString("Username"),
                rs.getString("Password"),
                rs.getString("Email"),
                rs.getString("Name"),
                rs.getString("PhoneNumber"),
                rs.getBigDecimal("Balance"),
                rs.getInt("RoleID"),
                rs.getDate("CreatedAt"),
                rs.getBoolean("IsDeleted"),
                rs.getString("DeletedBy"),
                rs.getString("CreatedBy")
            );
        }
    } catch (SQLException e) {
        System.out.println(e);
    }
    return null;
    }

    public boolean checkExist(String username, String email) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
