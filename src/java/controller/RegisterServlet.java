/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package coltroller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import vip.Email;
import vip.PasswordEncryption;

/**
 *
 * @author admin
 */
public class RegisterServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.getRequestDispatcher("views/user/homePage/register.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        

        String message = null;

        if (username != null && !username.isEmpty() &&
            email != null && !email.isEmpty() &&
            password != null && !password.isEmpty()) {

            // Kiểm tra xem username và email đã tồn tại chưa
            if (!DAO.INSTANCE.checkExist(username, email)) {
                // Tạo mã kích hoạt ngẫu nhiên
                String activationCode = PasswordEncryption.generateRandomCode();

                // Mã hóa mật khẩu trước khi lưu vào cơ sở dữ liệu
                String hashedPassword = PasswordEncryption.sha256Hash(password);

                // Thêm người dùng mới vào cơ sở dữ liệu với trạng thái kích hoạt là 0 (chưa kích hoạt)
                DAO.INSTANCE.register(username, email, hashedPassword, activationCode);

                // Gửi email kích hoạt
                String activationLink = "http://localhost:9999/Test/views/user/homePage/activeAccount.jsp?code=" + activationCode;
                String emailContent = "Chào mừng bạn đến với trang web của chúng tôi! Để kích hoạt tài khoản, vui lòng truy cập đường link sau: " + activationLink;
                boolean emailSent = Email.sendEmail(email, "Kích hoạt tài khoản", emailContent);

                if (emailSent) {
                    message = "Đăng ký thành công! Vui lòng kiểm tra email để kích hoạt tài khoản.";
                } else {
                    message = "Đăng ký thành công, nhưng không thể gửi email kích hoạt. Vui lòng thử lại sau.";
                }
            } else {
                message = "Tên đăng nhập hoặc email đã tồn tại, vui lòng chọn tên đăng nhập và email khác.";
            }
        } else {
            message = "Vui lòng điền đầy đủ thông tin và đảm bảo mật khẩu và xác nhận mật khẩu trùng khớp.";
        }

        request.setAttribute("message", message);
        doGet(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
