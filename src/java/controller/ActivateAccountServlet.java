package controller;

import dal.DAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ActivateAccountServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Lấy mã kích hoạt từ tham số trên URL
        String activationCode = request.getParameter("code");

        if (activationCode != null && !activationCode.isEmpty()) {
            // Kiểm tra xem mã kích hoạt có tồn tại trong cơ sở dữ liệu không
            if (DAO.INSTANCE.isActivationCodeValid(activationCode)) {
                // Kích hoạt tài khoản
                DAO.INSTANCE.activateAccount(activationCode);

                // Chuyển hướng đến trang đăng nhập hoặc trang thông báo kích hoạt thành công
                response.sendRedirect(request.getContextPath() + "/login");
            } else {
                // Trường hợp mã kích hoạt không hợp lệ
                response.sendRedirect(request.getContextPath() + "/");
            }
        } else {
            // Trường hợp không có mã kích hoạt
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Xử lý nếu cần thiết
    }
}
